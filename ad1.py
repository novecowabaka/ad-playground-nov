import matplotlib as mpl
from matplotlib import pyplot as plt
import numpy as np
from math import cos, sin


def fun(x, y, z):
    a1 = x * y
    a2 = cos(a1)
    a3 = a2 / a1
    a4 = a1 - z
    a5 = a3 / a4
    return a5


def dfun_reverse_ad(x, y, z):
    a1 = x * y
    a2 = cos(a1)
    a3 = a2 / a1
    a4 = a1 - z
    a5 = a3 / a4
    da5da5 = 1
    da5da4 = -a3 / a4**2
    da5da3 = 1 / a4
    da5da2 = da5da3 * 1 / a1
    da5da1 = da5da2 * -sin(a1) + da5da3 * -a2 / a1**2 + da5da4 * 1
    da5dz = da5da4 * -1
    da5dx = da5da1 * y
    da5dy = da5da1 * x
    return np.array([da5dx, da5dy, da5dz])


def dfun_numerical(x, y, z, h):
    funxyz = fun(x, y, z)
    dfdx = (fun(x + h, y, z) - funxyz) / h
    dfdy = (fun(x, y + h, z) - funxyz) / h
    dfdz = (fun(x, y, z + h) - funxyz) / h
    return np.array([dfdx, dfdy, dfdz])


def compare_diff(f1, f2, h, *args):
    a = f1(*args)
    b = f2(*args, h)
    return np.linalg.norm(a - b) / np.linalg.norm(a)


x = 1.5
y = 2.5
z = 3.5
h = 1e-8

ad_grad = dfun_reverse_ad(x, y, z)
numerical_grad = dfun_numerical(x, y, z, h)

print(compare_diff(dfun_reverse_ad, dfun_numerical, h, x, y, z))

mpl.rcParams['text.usetex'] = True
plt.rcParams['text.latex.preamble'] = r"\usepackage{bm} \usepackage{amsmath}"

hs = np.logspace(0, -20, 512)
errors = np.array(list(map(lambda h: compare_diff(
    dfun_reverse_ad, dfun_numerical, h, x, y, z), hs)))
plt.plot(hs, errors)
plt.xscale('log')
plt.yscale('log')
plt.title(r'Forward difference error vs step size')
plt.xlabel(r'$h$')
plt.ylabel(r'$||\text{error}||$')
plt.show()

# gradient descent


def fun_dfun(arr):
    x, y, z = arr
    a1 = x * y
    a2 = cos(a1)
    a3 = a2 / a1
    a4 = a1 - z
    a5 = a3 / a4
    da5da5 = 1
    da5da4 = -a3 / a4**2
    da5da3 = 1 / a4
    da5da2 = da5da3 * 1 / a1
    da5da1 = da5da2 * -sin(a1) + da5da3 * -a2 / a1**2 + da5da4 * 1
    da5dz = da5da4 * -1
    da5dx = da5da1 * y
    da5dy = da5da1 * x
    return a5, np.array([da5dx, da5dy, da5dz])


arg = np.array([x, y, z])
grad = np.zeros(3)
costs = np.array([])
dt = 1e-4
for i in range(100):
    newarg = arg - dt * grad
    cost, grad = fun_dfun(newarg)
    if (i > 0) and (cost > costs[-1]):
        dt = dt / 2
        continue
    arg = newarg
    costs = np.append(costs, cost)

print(f"Minimizer: {arg}")
plt.plot(costs)
plt.title(r'Gradient descent')
plt.xlabel(r'iteration')
plt.ylabel(r'$a_5$')
plt.show()
